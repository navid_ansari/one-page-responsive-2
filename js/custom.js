$(document).ready(function () {

    //carousel
    $("#carousel-example-generic").carousel({
        interval: 3000,
        pause: true
    });

    //accordian first collapse
    $('.collapse').collapse('hide');

    //sticky nav
    $('.work-section').waypoint(function (direction) {
        if (direction == "down") {
            $('.navigation-wrap').addClass('sticky');
        } else {
            $('.navigation-wrap').removeClass('sticky');
        }
    }, {
        offset: '60px;'

    });


    //scrollspy
    $(function () {
        "use strict";
        $(window).bind('scroll', function () {
            $('.anchor').each(function () {
                var anchored = $(this).attr("id"),
                    position = $(this).position().top - $(window).scrollTop(),
                    targetOffset = $(this).offset().top - 50;

                if ($(window).scrollTop() > targetOffset) {

                    $('ul.custom-navbar').find('li').removeClass("active");
                    $('ul.custom-navbar').find(('*[data-anchor="' + anchored + '"]')).addClass("active");
                }
            });
        });
    });

    //smooth scroll
    $(function () {
        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });


    //burger menu
    $('.menu-icon').click(function () {
        $('.custom-navbar').slideToggle(300);

        if ($('.menu-icon i').hasClass('fa-bars')) {

            $('.menu-icon i').addClass('fa-times');
            $('.menu-icon i').removeClass('fa-bars');

        } else {

            $('.menu-icon i').addClass('fa-bars');
            $('.menu-icon i').removeClass('fa-times');

        }

    });

    // hide navigation on click on body
    if (window.matchMedia('(max-width: 768px)').matches) {
        // do functionality on screens smaller than 768px

        $(document).on('click', function (e) {
            //console.log(e);
            if ($(e.target).closest(".mobile-nav-icon").length === 0) {

                $(".custom-navbar").slideUp("slow");

                $(".mobile-nav-icon i").hasClass("fa-times");
                $(".mobile-nav-icon i").addClass("fa-bars");
                $(".mobile-nav-icon i").removeClass("fa-times");
            }
        });
    }

    //change text size on scroll
    $(document).scroll(function () {
        var row = $('.logo-text'),
            scrollTop = $(this).scrollTop();
        if (scrollTop > 400) {

            $(".logo-text").css("font-size", "120%");
        } else {

            $(".logo-text").css("font-size", "150%");
        }

    });

    //animation on scroll // using animate.css
    $('.js-wp-1').waypoint(function (direction) {
        $('.js-wp-1').addClass('animated fadeIn');
    }, {
        offset: '50%'
    });

    $('.js-wp-2').waypoint(function (direction) {
        $('.js-wp-2').addClass('animated fadeIn');
    }, {
        offset: '50%'
    });

    $('.js-wp-3').waypoint(function (direction) {
        $('.js-wp-3').addClass('animated fadeIn');
    }, {
        offset: '50%'
    });

    $('.js-wp-4').waypoint(function (direction) {
        $('.js-wp-4').addClass('animated fadeIn');
    }, {
        offset: '50%'
    });

    $('.js-wp-5').waypoint(function (direction) {
        $('.js-wp-5').addClass('animated slideInUp');
    }, {
        offset: '50%'
    });

    new WOW().init();
    
    //scroll spy navigation 2
    $(document).on("scroll", onScroll);

    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top + 2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });

});

//scroll spy navigation 2 // use nav element for main navigation and dot navigation //use same id for both nav elements
function onScroll(event) {
    var scrollPosition = $(document).scrollTop();
    $('nav').each(function () {
        var currentLink = $(this);
        var refElement = $(currentLink.attr("href"));
        if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
            $('nav ul li').removeClass("active");
            currentLink.addClass("active");
        } else {
            currentLink.removeClass("active");
        }
    });
}